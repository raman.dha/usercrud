<!DOCTYPE html>
    <html>
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    	<title>User CRUD</title>
    	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bootstrap_css/css/bootstrap.min.css">
    </head>
    <body>
    <div class="container">
    	<h1 class="page-header text-center">User CRUD</h1>
    	<div class="row">
    		<div class="col-sm-10 col-sm-offset-0">
				<div class="col-sm-7 col-sm-offset-7">
    				<a href="<?php echo base_url(); ?>index.php/users/addnew" class="btn btn-info"><span class="glyphicon glyphicon-user"></span> Add New User </a><br><br>
				</div>
    			<table class="table table-bordered table-striped">
    				<thead>
    					<tr>
    						<th>ID</th>
    						<th>Username</th>
    						<th>User Role</th>
    						<th>EMail</th>
							<th>Address</th>
    						<th>Action</th>
    					</tr>
    				</thead>
    				<tbody>
    					<?php
						foreach($users as $user)
						{
    						?>
    						<tr>
    							<td><?php echo $user->user_id; ?></td>
    							<td><?php echo $user->username; ?></td>
    							<td><?php echo $user->label; ?></td>
    							<td><?php echo $user->email; ?></td>
								<td><?php echo $user->address . ', ' . $user->city . ', '. $user->province.', ' . $user->country . '. ' . strtoupper($user->postal_code); ?></td>
    							<td><a href="<?php echo base_url();  ?>index.php/users/edit/<?php echo $user->user_id;  ?>" class="btn btn-warning"><span class="glyphicon glyphicon-pencil"></span> Edit</a>    <a href="<?php echo base_url();  ?>index.php/users/delete/<?php echo $user->user_id;  ?>" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Delete</a></td>
    						</tr>
    						<?php
    					}
    					?>
    				</tbody>
    			</table>
    		</div>
    	</div>
    </div>
    </body>
    </html>