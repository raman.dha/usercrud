

    <!DOCTYPE html>
    <html>
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    	<title>User CRUD</title>
    	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bootstrap_css/css/bootstrap.min.css">
    </head>
    <body>
    <div class="container">
    	<h1 class="page-header text-center">User CRUD</h1>
    	<div class="row">
    		<div class="col-sm-4 col-sm-offset-4">
    			<h3>Add Form
    				<span class="pull-right"><a href="<?php echo base_url(); ?>" class="btn btn-primary"><span class="glyphicon glyphicon-chevron-left"></span> Back</a></span>
    			</h3>
    			<hr>
    			<form name = "user_form" method="POST" action="<?php echo base_url(); ?>index.php/users/insert">
    				<div class="form-group">
    					<label>Username:</label>
    					<input type="text" class="form-control" name="username">
    				</div>
    				<div class="form-group">
    					<label>User Role ID:</label>
    					<input type="text" class="form-control" name="user_role_id" placeholder = "1-3">
    				</div>
    				<div class="form-group">
    					<label>EMail:</label>
    					<input type="text" class="form-control" name="email">
    				</div>
                    <div class="form-group">
    					<label>Street Address:</label>
    					<input type="text" class="form-control" name="address">
    				</div>
                    <div class="form-group">
    					<label>City:</label>
    					<input type="text" class="form-control" name="city">
    				</div>
					<div class="form-group">
    					<label>Province:</label>
    					<input type="text" class="form-control" name="province">
    				</div>
                    <div class="form-group">
    					<label>Country:</label>
    					<input type="text" class="form-control" name="country">
    				</div>
                    <div class="form-group">
    					<label>Postal Code:</label>
    					<input type="text" class="form-control" name="postal_code">
    				</div>
    				<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Add User</button>
    			</form>
    		</div>
    	</div>
    </div>
    </body>
    </html>