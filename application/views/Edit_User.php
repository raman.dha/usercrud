<!DOCTYPE html>
    <html>
    <head>
    	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    	<title>User CRUD</title>
    	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>bootstrap_css/css/bootstrap.min.css">
    </head>
    <body>
    <div class="container">
    	<h1 class="page-header text-center">User CRUD</h1>
    	<div class="row">
    		<div class="col-sm-4 col-sm-offset-4">
    			<h3>Edit Form
    				<span class="pull-right"><a href="<?php echo base_url(); ?>" class="btn btn-primary"><span class="glyphicon glyphicon-chevron-left"></span> Back</a></span>
    			</h3>
    			<hr>
    			<?php extract($user);?>
    			<form method="POST" action="<?php echo base_url(); ?>index.php/users/update/<?php echo $user_id; ?>">
    				<div class="form-group">
    					<label>Username:</label>
    					<input type="text" class="form-control" value="<?php echo $username; ?>" name="username">
    				</div>
    				<div class="form-group">
    					<label>User Role ID:</label>
    					<input type="text" class="form-control" value="<?php echo $user_roles_id; ?>" name="user_role_id" placeholder = "1-3">
    				</div>
    				<div class="form-group">
    					<label>EMail:</label>
    					<input type="text" class="form-control" value="<?php echo $email; ?>" name="email">
    				</div>
                    <div class="form-group">
    					<label>Street Address:</label>
    					<input type="text" class="form-control" value="<?php echo $address; ?>" name="address">
    				</div>
                    <div class="form-group">
    					<label>City:</label>
    					<input type="text" class="form-control" value="<?php echo $city; ?>" name="city">
    				</div>
                    <div class="form-group">
    					<label>Province:</label>
    					<input type="text" class="form-control" value="<?php echo $province; ?>" name="province">
    				</div>
					<div class="form-group">
    					<label>Country:</label>
    					<input type="text" class="form-control" value="<?php echo $country; ?>" name="country">
    				</div>
                    <div class="form-group">
    					<label>Postal Code:</label>
    					<input type="text" class="form-control" value="<?php echo $postal_code; ?>" name="postal_code">
    				</div>
    				<button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Update</button>
    			</form>
    		</div>
    	</div>
    </div>
    </body>
    </html>