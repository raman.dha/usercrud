    <?php
        class User_Model extends CI_Model 
        {
            function __construct()
            {
    			parent::__construct();
    			$this->load->database();
            }

            public function getAllUsers()
            {
                $this->db->select('*');
                $this->db->from('users');
                $this->db->join('user_addresses', 'user_addresses.user_id = users.id');
                $this->db->join('user_roles', 'users.user_roles_id = user_roles.id');
                $query = $this->db->get();
                
                return $query->result();
            }
            
            public function getRoleName()
            {
    			$query = $this->db->get('user_roles');
    			return $query->result(); 
    		}

            public function insertuser($user, $useraddress)
            {
                $this->db->trans_start();

                $this->db->insert('users', $user);
                
                $useraddress['user_id'] =  $this->db->insert_id();

                $this->db->insert('user_addresses', $useraddress);

                return $this->db->trans_complete();
    		}
     
            public function getUser($id)
            {
                $this->db->select('*');
                $this->db->from('users');
                $this->db->join('user_addresses', 'user_addresses.user_id = users.id');
                $this->db->join('user_roles', 'users.user_roles_id = user_roles.id');
                $this->db->where('users.id', $id);
                $query = $this->db->get();

    			return $query->row_array();
    		}
     
            public function updateuser($user, $id)
            {
                   $adata = array(
                        'username'      => $user['username'],
                        'user_roles_id' => $user['user_roles_id'],
                        'email'         => $user['email'],
                        'updated_at'    => date('Y-m-d H:i:s')
                    );

                $this->db->trans_start();

                $this->db->where('users.id', $id);
                $this->db->update('users', $adata);

                $bdata = array(
                    'address'      => $user['address'],
                    'city'         => $user['city'],
                    'province'     => $user['province'],
                    'postal_code'  => $user['postal_code']
                );
            
                $this->db->where('user_addresses.user_id', $id);
                $this->db->update('user_addresses', $bdata);

                return $this->db->trans_complete();
    		}
     
            public function deleteuser($id)
            {
                $this->db->trans_start();

                $this->db->where('users.id', $id);
                $this->db->delete('users');
                
                $this->db->where('user_addresses.user_id', $id);
                $this->db->delete('user_addresses');
                
                return $this->db->trans_complete();
    		}
     
    	}
    ?>