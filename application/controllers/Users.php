<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
     
    class Users extends CI_Controller
    {
        function __construct()
        {
    		parent::__construct();
    		$this->load->helper('url');
    		$this->load->model('User_Model');
    	}
     
        public function index()
        {
    		$data['users'] = $this->User_Model->getAllUsers();
    		$this->load->view('user_list.php', $data);
        }
        
        public function addnew()
        {
    		$this->load->view('Add_User.php');
    	}
     
        public function insert()
        {
    		$user['username'] = $this->input->post('username');
    		$user['user_roles_id'] = $this->input->post('user_role_id');
            $user['email'] = $this->input->post('email');
            $user['created_at'] = date('Y-m-d H:i:s');

            $useraddress['address'] = $this->input->post('address');
            $useraddress['city'] = $this->input->post('city');
            $useraddress['province'] = $this->input->post('province');
            $useraddress['country'] = $this->input->post('country');
            $useraddress['postal_code'] = $this->input->post('postal_code');
            
    		$query = $this->User_Model->insertuser($user, $useraddress);
     
            if($query)
            {
    			header('location:'.base_url().$this->index());
    		}
    	}
     
        public function edit($id)
        {
            $data['user'] = $this->User_Model->getUser($id);
            $this->load->view('Edit_User', $data);
    	}
     
        public function update($id)
        {
    		$user['username'] = $this->input->post('username');
    		$user['user_roles_id'] = $this->input->post('user_role_id');
            $user['email'] = $this->input->post('email');
            $user['address'] = $this->input->post('address');
    		$user['city'] = $this->input->post('city');
            $user['province'] = $this->input->post('province');
            $user['postal_code'] = $this->input->post('postal_code');
            $user['country'] = $this->input->post('country');

    		$query = $this->User_Model->updateuser($user, $id);
     
            if($query)
            {
    			header('location:'.base_url().$this->index());
    		}
    	}
     
        public function delete($id)
        {
    		$query = $this->User_Model->deleteuser($id);
     
            if($query)
            {
    			header('location:'.base_url().$this->index());
    		}
    	}
    }

?>