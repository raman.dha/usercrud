<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
     
    class API extends CI_Controller
    {
        function __construct()
        {
    		parent::__construct();
    		$this->load->helper('url');
    		$this->load->model('User_Model');
    	}
     
        public function list()
        {
            $data['users'] = $this->User_Model->getAllUsers();
            foreach($data as $user)
            {
                echo(json_encode($user));
            }
        }

        public function single_user($id)
        {
            $data['user'] = $this->User_Model->getUser($id);

            echo(json_encode($data));
        }

        public function add_user()
        {
            $post_data = json_decode(file_get_contents("php://input"), true);

            $user['username'] = $post_data['username'];
    		$user['user_roles_id'] = $post_data['user_role_id'];
            $user['email'] = $post_data['email'];
            $user['created_at'] = date('Y-m-d H:i:s');

            $useraddress['address'] = $post_data['address'];
            $useraddress['city'] = $post_data['city'];
            $useraddress['province'] = $post_data['province'];
            $useraddress['country'] = $post_data['country'];
            $useraddress['postal_code'] = $post_data['postal_code'];
            
            $query = $this->User_Model->insertuser($user, $useraddress);
            
            if($query)
                echo("200:Success! User Created.");
            else
                echo("400:Unable to create a user, please try again!");
        }

        public function edit_user($id)
        {
            $post_data = json_decode(file_get_contents("php://input"), true);

            $user['username'] = $post_data['username'];
    		$user['user_roles_id'] = $post_data['user_role_id'];
            $user['email'] = $post_data['email'];
            $user['address'] = $post_data['address'];
            $user['city'] = $post_data['city'];
            $user['province'] = $post_data['province'];
            $user['country'] = $post_data['country'];
            $user['postal_code'] = $post_data['postal_code'];

            $query = $this->User_Model->updateuser($user, $id);
            
            if($query)
                echo("200:Success! User Updated.");
            else
                echo("400:Unable to edit user, please try again!");
        }

        public function delete_user($id)
        {
            $query = $this->User_Model->deleteuser($id);
     
            if($query)
    			echo("200:Success! User Deleted.");
            else
                echo("400:Unable to delete the  user, please try again!");
        }

    }
?>